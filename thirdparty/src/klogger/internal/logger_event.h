#ifndef LOGGER_EVENT_H
#define LOGGER_EVENT_H

class Appender;

struct LoggerEvent {
  int type;
  void *appender;
};

#endif // LOGGER_EVENT_H
