/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef GRAMMAR_H
#define GRAMMAR_H

#include "kconfig/interface/config.h"

class Lexer;
class Domain;
class FileManager;
class File;

using namespace kconfig;

/**
 * @brief grammar
 */
class Grammar {
public:
  /**
   * @brief ctor
   */
  Grammar(Config *config);

  /**
   * @brief dtor
   */
  ~Grammar();

  /**
   * @brief do parsing
   */
  Domain *parse(const std::string &base, const std::string &path);

  /**
   * @brief check whether parsing is ok
   */
  bool isOk();

  /**
   * @brief get last error string
   */
  std::string getLastError();

  /**
   * 获取根域.
   * 
   * \return 
   */
  Domain *get_root_domain();

  /**
   * 获取配置指针.
   * 
   * \return 配置指针
   */
  Config *get_config();

private:
  /**
   * @brief set error string
   */
  void error(const std::string &reason);

  /**
   * @brief do 'chuck'
   */
  void chunk();

  /**
   * @brief do 'require'
   */
  void require();

  /**
   * @brief do 'statement'
   */
  void statement();

  /**
   * @brief do 'expression'.
   *
   */
  void expression(Attribute *parent, const std::string &name);

  /**
   * 条件表达式产生的值.
   */
  void expression_value();

  /**
   * @brief do 'domain'
   */
  void domain(const std::string &name);

  /**
   * 匿名域.
   *
   */
  void anonymous_domain(Domain* parent_domain);

  /**
   * @brief do 'array'
   */
  void array(Attribute *parent, const std::string &name);

  /**
   * @brief do 'table'
   */
  void table(Attribute *parent, const std::string &name);

  /**
   * @brief do 'table pair'
   */
  void pair(Attribute *parent);

  /**
   * @brief do 'string'
   */
  void string(Attribute *parent, const std::string &key);

  /**
   * @brief do 'number'
   */
  void number(Attribute *parent, const std::string &key,
              const std::string &name);

  /**
   * @brief do 'bool'
   */
  void bool_literal(Attribute *parent, const std::string &key,
                    const std::string &name);

private:
  Domain *_root;              // root domain
  Domain *_domain;            // current domain
  Lexer *_lexer;              // current lexer
  File *_file;                // current file
  bool _error;                // error flag
  std::string _lastError;     // error string
  std::string _base;          // base file path
  FileManager *_fileManager;  // file manager
  Config *_config;            // Config
  AttributePtr _cur_expr_val; // 当前的表达式值
};

//
// helper macro
//

#define ErrorReturn(reason)                                                    \
  error(reason);                                                               \
  return;

#define ErrorReturnNull(reason)                                                \
  error(reason);                                                               \
  return 0;

#define ErrorReturnValue(reason, value)                                        \
  error(reason);                                                               \
  return value;

#endif // GRAMMAR_H
