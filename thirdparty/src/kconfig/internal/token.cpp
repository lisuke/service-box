/*
 * Copyright (c) 2013-2015, dennis wang
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "token.h"

Token::Token(int type, int fileId, int row, int column, const char *literal) {
  _type = type;
  _fileId = fileId;
  _column = column;
  _row = row;

  if (0 != literal) {
    _text = literal;
  }
}

Token::~Token() {
  _type = 0;
  _fileId = 0;
  _column = 0;
  _row = 0;
  _text = "";
}

int Token::type() { return _type; }

int Token::getFileId() { return _fileId; }

int Token::getRow() { return _row; }

int Token::getColumn() { return _column; }

const std::string &Token::getText() { return _text; }

bool Token::isTerm(int ch) {
  switch (ch) {
  case '=':
  case '{':
  case '}':
  case '(':
  case ')':
  case '<':
  case '>':
  case ',':
  case ':':
  case '+':
  case '-':
  case '*':
  case '/':
  case '`':
  case '!':
  case '%':
  case '&':
  case '|':
  case '[':
  case ']':
    return true;
  }

  return false;
}

int Token::getTokenType(int ch) {
  switch (ch) {
  case '=':
    return EQUAL;

  case '{':
    return DOMAIN_START;

  case '}':
    return DOMAIN_END;

  case '(':
    return ARRAY_START;

  case ')':
    return ARRAY_END;

  case '<':
    return TABLE_START;

  case '>':
    return TABLE_END;

  case '"':
    return QUOTE;

  case ',':
    return COMMA;

  case ':':
    return COLON;

  case '+':
    return PLUS;

  case '-':
    return SUB;

  case '*':
    return MUL;

  case '/':
    return DIV;

  case '`':
    return SLASH_STROPHE;

  case '!':
    return EXCL_MARK;

  case '%':
    return MOD;

  case '&':
    return AND;

  case '|':
    return OR;

  case '[':
    return LSQUARE;

  case ']':
    return RSQUARE;
  }

  return 0;
}
