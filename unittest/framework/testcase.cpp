#include "testcase.hh"

namespace k {
namespace test {

Testcase::Testcase(const std::string & name)
    : name_(name), run_(true) {
}

Testcase::~Testcase() {
}

const std::string & Testcase::getName() const {
    return name_;
}

void Testcase::disable() {
    run_ = false;
}

void Testcase::enable() {
    run_ = true;
}

bool Testcase::isEnable() {
    return run_;
}

bool Testcase::isSuccess() {
    return errorList_.empty();
}

void Testcase::setError(const std::string & error) {
    errorList_.push_back(error);
}

const ErrorList & Testcase::getErrorList() const {
    return errorList_;
}

void Testcase::clearError() {
    errorList_.clear();
}

}
}
