# 配置service box

# 配置节点

1. listener
2. service_finder
3. service
4. command
5. debugger
6. statistics
7. proxy
8. http
9. 全局属性

## listener
1. listener.host

服务器容器监听的地址:端口,用于与其他服务容器建立连接
```
listener = {
    host = ("0.0.0.0:6888")
}
```
可以配置多个端口和地址
```
listener = {
    host = ("0.0.0.0:6888","0.0.0.0:7888")
}
```
## service_finder
第三方服务发现/注册相关配置

1. service_finder.type

   第三方服务类型,目前只支持zookeeper
2. service_finder.hosts

   第三方服务地址:端口
```
service_finder = {
    type = "zookeeper"
    hosts = "127.0.0.1:2181"
}
```

## service
1. service.service_dir<br>
   用于存放本地可加载服务的目录,如果配置相对目录则为相对启动程序所在目录的相对路径
2. service.preload_service<br>
   需要加载的服务
3. service.remote_service_repo_version_api<br>
   存放服务的远程库版本信息WebService API
4. service.remote_service_repo_dir<br>
   存放服务的远程仓库
5. service.remote_service_repo_latest_version_api<br>
   存放服务的远程最新库版本信息WebService API
6. service.is_open_remote_update<br>
   是否开启远程更新
7. service.remote_repo_check_interval<br>
   检查远程更新的时间间隔，秒
8. service.is_start_as_daemon<br>
   是否以守护进程的方式启动

版本信息文件内容如下:

```
{
  "version" : "1.0",
  "bundles" : [
    {
        "uuid":"5871407834711899994",
        "name":"libServiceDynamic.so"
    }
  ]
} 
```
1. version<br>
   版本号
2. bundles<br>
   多个服务
3. uuid<br>
   服务的UUID
4. name<br>
   存放服务的.so文件名

配置文件示例如下:
```
service = {
    service_dir="../../service/Debug"
    preload_service=<5871407834711899994:"libServiceDynamic.so",5871407834537456905:"libLogin.so",5871407834107390365:"libMyService.so",5871407833380299500:"libLua.so">
    remote_service_repo_version_api = "http://localhost/doc/version.json"
    remote_service_repo_dir = "http://localhost/bundle_repo"
    remote_service_repo_latest_version_api = "http://localhost/doc/version.json"
    is_open_remote_update = "false"
    remote_repo_check_interval = 60
    is_start_as_daemon = "false"
}
```

## command
自定义命令相关配置,配置开启后可以向容器发送命令并调用由用户编写的命令功能

1. command.enable<br>
   是否开启命令服务
2. command.listen<br>
   命令服务监听的IP:端口
3. command.root_page_path<br>
   用于Console访问的网页存放的路径
```
command = {
    enable = "true"
    listen = "0.0.0.0:6889"
    root_page_path = "root.html"
}

```

## debugger
脚本调试器相关配置,通过Console可以访问脚本调试服务

1. debugger.enable<br>
   是否开启脚本调试服务
2. debugger.listen<br>
   脚本调试服务监听的IP:端口

```
debugger = {
    enable = "true"
    listen = "0.0.0.0:10888"
}
```

## statistics
1. statistics.is_open_rpc_stat<br>

```
statistics = {
    is_open_rpc_stat = "true"
}
```

## proxy
当以代理模式启动时需要的配置
1. proxy.listener<br>
   代理模式启动时监听的IP:端口
2. proxy.seed<br>
   代理模式启动时的种子,不能与其他代理的种子重复
3. proxy.query_timeout<br>
   代理模式启动时,发现集群内服务的超时时间，毫秒
4. proxy.call_timeout<br>
   代理模式启动时,调用集群内服务的超时时间,毫秒
```
proxy = {
    query_timeout = 5000
    call_timeout = 2000
    seed = 1
    listener = ("0.0.0.0:9888")
}
```
## http
HTTP相关配置
1. http.http_max_call_timeout<br>
   HTTP调用超时,秒
```
http = {
    http.http_max_call_timeout = 5000
}
```

## 全局属性
1. box_channel_recv_buffer_len<br>
   网络管道接收缓冲区初始大小,字节
2. box_name<br>
   服务容器名字
3. logger_config_line<br>
   日志配置
4. connect_other_box_timeout<br>
   连接其他服务容器时的超时间隔,秒
5. service_finder_connect_timeout<br>
   连接第三方服务发现超时间隔,秒
6. necessary_service<br>
   以来集群内必须有的服务名(即已经注册了)
7. open_coroutine<br>
   是否开启协程
```
box_channel_recv_buffer_len = 102400
box_name = "service_box"
logger_config_line = "file://.;file=box_%y%m%d.log;line=[%y%m%d-%H:%M:%S:%U];flush=true;mode=day;maxSize=10240000"
connect_other_box_timeout = 2000
service_finder_connect_timeout = 2000
necessary_service = ("service1", "service2")
open_coroutine = "true"
```

### 日志配置
日志配置为全局属性，属性名为`logger_config_line`, 配置方式为：
1. 文件型<br>
   "file://.;file=box_%y%m%d.log;line=[%y%m%d-%H:%M:%S:%U];flush=true;mode=day;maxSize=10240000"
2. 控制台<br>
   "console://.;line=[%y%m%d-%H:%M:%S:%U];flush=true"

### 调用链跟踪配置
1. open_trace<br>
   开启调用链跟踪组件
2. call_trace.type<br>
   第三方类型
3. call_trace.reporter.zipkin<br>
   Zipkin服务的地址
```
open_trace = true
call_tracer = {
    type = "zipkin"
    reporter = {
        zipkin = "192.168.103.241:9411"
    }
}

```

### 模块
1. module.module_dir<br>
   模块存放的目录

```
module = {
	module_dir="your_dir"
}
```

### 流量控制
1. limit.open_limit<br>
   开启流量控制
2. limit.{服务名}.bucket_volume
   令牌桶最大容量
3. limit.{服务名}.token_per_sec
   每秒产生的令牌数量
4. limit.{服务名}.type
   令牌产生器类型

```
limit = {
    open_limit = true
    YourServiceName = {
        bucket_volume = 10000
        token_per_sec = 1000
        type = "default"
    }
}
```

### 表达式

表达式是可以运行时计算的配置属性，一个简单的例子:

```
a = `1+2`
```
等价于
```
a = 3
```
表达式的用途是可以根据运行的环境来确定实际的配置值，表达式也可以作为条件配置的开关，譬如：
```
`os_name() == "linux"` {
    service_dir="../service"
}
```
以上的配置表示，在linux平台下配置service_dir属性为一个特殊的值，表达式中os_name为表达式函数，现在支持多种表达式函数:
| 函数  | 功能   |
|---|---|
|  os_name | 获取操作系统名称  |
|  print |  在屏幕输出，支持多个参数 |
|is_debug|检测是否是调试版本|
|is_realse|检测是否是发行版本|
|cwd|获取当前的工作目录|
|...||

表达式还支持预定义的变量：
| 变量名  | 功能   |
|---|---|
|K|1k=1024|
|M|1m=1024k|
|G|1g=1024m|
|...||

```
a = `100*K`

以上表达式等价于
a = `100*1024`
```
表达式以`作为开始和结尾，表达式函数和变量只能在表达式内使用。 