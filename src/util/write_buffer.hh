#pragma once

#include <cstdlib>
#include <memory>
#include <utility>

namespace kratos {
namespace util {

/**
 * 写入缓冲区.
 */
class WriteBuffer {
  std::unique_ptr<char[]> buf_{nullptr}; ///< 数据指针
  std::size_t length_{0};                ///< 缓冲区长度
  std::size_t size_{0};                  ///< 数据长度

public:
  /**
   * 构造.
   *
   * \param capacity 缓冲区预设容量, 可动态增长
   */
  WriteBuffer(std::size_t capacity = 1024 * 4);
  /**
   * 析构.
   *
   */
  ~WriteBuffer();
  /**
   * 写入数据.
   *
   * \param data 数据指针
   * \param length 数据长度
   * \return <在缓冲区内地址, 长度>
   */
  auto write(const char *data, std::size_t length)
      -> std::pair<const char *, std::size_t>;
  /**
   * 申请一块空间.
   * 
   * \param length 申请的长度
   * \return <在缓冲区内地址, 长度>
   */
  auto apply(std::size_t length) ->std::pair<char*, std::size_t>;
  /**
   * 返回数据总长度.
   *
   * \return
   */
  auto length() -> std::size_t;
  /**
   * 获取缓冲区起始地址.
   *
   * \return
   */
  auto get() -> const char *;
  /**
   * 清理缓存.
   * 
   * \return 
   */
  auto clear() -> void;
};
} // namespace util
} // namespace kratos
