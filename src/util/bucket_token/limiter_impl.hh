#pragma once

#include "bucket_token_defines.hh"
#include "root/rpc_root.h"
#include <unordered_map>

namespace kratos {
namespace config {
struct LimitConfig;
}
} // namespace kratos

namespace kratos {
namespace service {

class ServiceBox;

/**
 * 限流器接口实现.
 */
class LimiterImpl : public rpc::Limiter {
  util::TokenBucketPtr token_bucket_ptr_;                    ///< 桶
  ServiceBox *box_{nullptr};                                 ///< 容器
  rpc::ServiceUUID service_uuid_{rpc::INVALID_SERVICE_UUID}; ///< Service UUID

public:
  /**
   * 构造.
   *
   * \param box 容器
   */
  LimiterImpl(ServiceBox *box);
  virtual ~LimiterImpl();
  // 是否被限流
  virtual auto is_limit() -> bool override;

public:
  /**
   * 启动.
   * \param limit_config  配置
   * \return true成功, false失败b
   */
  auto start(const config::LimitConfig &limit_config) -> bool;
  /**
   * 获取服务UUID.
   */
  auto get_service_uuid() -> rpc::ServiceUUID;
  /**
   * 主循环.
   *
   * \param now 当前时间戳
   * \return
   */
  auto update(std::time_t now) -> void;
};

using LimiterImplPtr = std::shared_ptr<LimiterImpl>;

/**
 * 限流器管理器.
 */
class LimiterManager {
  using LimiterMap = std::unordered_map<rpc::ServiceUUID, LimiterImplPtr>;
  LimiterMap limiter_map_;   ///< {ServiceUUID, LimiterImplPtr}
  ServiceBox *box_{nullptr}; ///< 容器

public:
  /**
   * 构造.
   */
  LimiterManager(ServiceBox *box);
  /**
   * 析构.
   */
  ~LimiterManager();
  /**
   * 启动s.
   */
  auto start() -> bool;
  /**
   * 关闭.
   */
  auto stop() -> void;
  /**
   * 主循环.
   *
   * \param now 当前时间戳
   * \return
   */
  auto update(std::time_t now) -> void;
};

using LimiterManagerPtr = std::shared_ptr<LimiterManager>;

} // namespace service
} // namespace kratos
