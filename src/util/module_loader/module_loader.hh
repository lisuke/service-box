#pragma once

#include "root/rpc_defines.h"
#include <string>

namespace kratos {
namespace service {
class ServiceBox;
}
} // namespace kratos

namespace rpc {
class Rpc;
}

/**
 * 模块导出函数.
 */
using ModuleExportFunc = void (*)(rpc::Rpc *);

namespace kratos {
namespace util {

/**
 * 模块加载器.
 */
class ModuleLoader {
  ModuleHandle handle_{INVALID_MODULE_HANDLE}; ///< 模块句柄
  rpc::Rpc *rpc_{nullptr};                     ///< RPC框架

  ModuleLoader(const ModuleLoader&) = delete;
  const ModuleLoader& operator=(const ModuleLoader&) = delete;

public:
  /**
   * 构造.
   *
   * \param rpc RPC框架指针
   */
  ModuleLoader(rpc::Rpc *rpc);
  /**
   * 右值构造.
   * 
   * \param rht 右值
   */
  ModuleLoader(ModuleLoader&& rht) noexcept(true);
  /**
   * 析构.
   *
   */
  ~ModuleLoader();
  /**
   * 加载模块s.
   *
   * \param module_path 模块路径
   * \return true成功，false失败
   */
  auto load(const std::string &module_path) -> bool;
  /**
   * 卸载.
   *
   * \return
   */
  auto unload() -> void;
  /**
   * 注册.
   */
  auto do_register_module() noexcept(false) -> bool;
  /**
   * 获取导出函数.
   *
   * \param func_name 函数名
   * \return 函数地址
   */
  auto get_export_func(const std::string &func_name) -> ModuleExportFunc;
  /**
   * 获取导出函数.
   *
   * \param func_name 函数名
   * \return 函数地址
   */
  auto get_export_func_raw(const std::string& func_name) -> void*;
};

} // namespace util
} // namespace kratos
