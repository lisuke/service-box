#include "module_loader.hh"

#if !defined(DISABLE_SB_CODE)
#include "util/os_util.hh"
#endif

namespace kratos {
namespace util {

ModuleLoader::ModuleLoader(rpc::Rpc *rpc) { rpc_ = rpc; }

ModuleLoader::ModuleLoader(ModuleLoader &&rht) noexcept(true) {
  handle_ = rht.handle_;
  rpc_ = rht.rpc_;
  rht.handle_ = INVALID_MODULE_HANDLE;
}

ModuleLoader::~ModuleLoader() { unload(); }

auto ModuleLoader::load(const std::string &module_path) -> bool {
#if defined(_WIN32) || defined(_WIN64)
  handle_ = ::LoadLibraryA(module_path.c_str());
#else
  handle_ = dlopen(module_path.c_str(), RTLD_LAZY);
#endif // defined(_WIN32) || defined(_WIN64)
  return (handle_ != INVALID_MODULE_HANDLE);
}

auto ModuleLoader::unload() -> void {
  if (handle_ == INVALID_MODULE_HANDLE) {
    return;
  }
#if defined(_WIN32) || defined(_WIN64)
  if (FALSE == ::FreeLibrary(handle_)) {
    if (FALSE == ::UnmapViewOfFile(handle_)) {
      return;
    }
  }
#else
  dlclose(handle_);
#endif // defined(_WIN32) || defined(_WIN64)
  handle_ = INVALID_MODULE_HANDLE;
}

auto ModuleLoader::do_register_module() noexcept(false) -> bool {
  ModuleExportFunc func{nullptr};
  // ע��rpc::ProxyCreator
  func = get_export_func("registerProxyCreator");
  if (!func) {
    // ע��rpc::StubCreator
    func = get_export_func("registerStubCreator");
  }
  if (!func) {
    return false;
  }
  func(rpc_);
  return true;
}

auto ModuleLoader::get_export_func(const std::string &func_name)
    -> ModuleExportFunc {
  if (handle_ == INVALID_MODULE_HANDLE) {
    return nullptr;
  }
  ModuleExportFunc reg_func{nullptr};
#if defined(_WIN32) || defined(_WIN64)
  reg_func = (ModuleExportFunc)::GetProcAddress(handle_, func_name.c_str());
#else
  reg_func = (ModuleExportFunc)dlsym(handle_, func_name.c_str());
#endif // defined(_WIN32) || defined(_WIN64)
  return reg_func;
}

auto ModuleLoader::get_export_func_raw(const std::string &func_name) -> void * {
  if (handle_ == INVALID_MODULE_HANDLE) {
    return nullptr;
  }
  void *reg_func{nullptr};
#if defined(_WIN32) || defined(_WIN64)
  reg_func = ::GetProcAddress(handle_, func_name.c_str());
#else
  reg_func = dlsym(handle_, func_name.c_str());
#endif // defined(_WIN32) || defined(_WIN64)
  return reg_func;
}

} // namespace util
} // namespace kratos
