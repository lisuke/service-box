# 外部组件

组件系统的设计是希望能自由的扩展进程内调用的高效代码模块的同时，又能减少对`ServiceBox`代码的更改，保持`ServiceBox`代码框架的相对稳定性. 组件可以被`ServiceBox`框架调用，也可以被用户服务调用(通过`ServiceContext`)，因为组件可以分为两大类:

1. 用户组件
2. 系统组件

用户组件的设计目标是提供给用户服务使用，框架也可以调用用户组件. 系统组件是被框架使用的. 组件是以二进制的`DLL/SO`的方式提供的，并导出了特定的函数，外部调用组件功能是通过调用这些函数来实现的.

# 组件的架构

组件的架构是松散的，通过约定目录结构和配置，任何一个`DLL/SO`都可以成为组件，即任何一个组件都有唯一的一个入口`DLL/SO`，`DLL/SO`所依赖的其他运行环境都通过配置一起发布，`ServiceBox`是不关心这些依赖运行环境是如何被加载的，这些都由组件自身进行管理.

## 组件加载

组件只有被存放在指定的目录(通过配置)下才会加载，但是组件可以存放在远程并被下载，下载到本地目录后被框架加载.

## 组件的命名空间

组件使用一个单一的命名空间，任何组件都有唯一的名字，不同组件不能重名, 组件是通过这个唯一名字被搜索和加载的.

## 组件版本

组件的版本号构成如下所示:

`major.minor.patch`

`major`、`minor`、`patch`都是数字，都是逐渐增长的.

# 组件加载流程

```
 + ------------------------------------------------------------- +
 |          pack.json@remote ------ref---> META.json@remote      |
 |               |      \                  |                     |
 |   REMOTE      |       `---------ref-----|----> binary@remote  |
 |               |                         |           |         |
 |           DOWNLOAD                   DOWNLOAD   DOWNLOAD      |
 |               |                         |           |         |
 + --------------)-------------------------)-----------)-------- +
 |               |                         |           |         |
 |              \/                         |           |         |
 |          pack.json@local                |           |         |  
 |               |     \                  \/           |         |
 |   LOCAL       |      `----ref---> META.json@local   |         |
 |               |                                    \/         |
 |                `------------ref--------------> binary@local   |
 |                                                               |
 + ------------------------------------------------------------- +
```

# 组件的配置

## 本地组件的目录结构

```
根目录
    \
     + -- 组件A
     |       \
     |        + -- temp
     |        |       \
     |        |        + -- pack.json, 只保留最新下载的
     |        + -- 版本A
     |        |        \
     |        |         + -- META.json
     |        |         |
     |        |         + -- ...
     |        + -- ...
     + -- ...
```

## `META.json`

配置文件名大小写敏感

```
{
   "version" : "1.0.1",
   "is_system" : false,
   "descriptor" : { "key":"value" },
   "entry_module" : "test.so",
   "deps" : [
     {
       "version" : ">=1.0.1",
       "name" : "test1",
       "source" : "http://127.0.0.1/doc/test1/pack-1.0.1.json"
     }
   ]
}
```
1. `version` 版本号
2. `is_system` 是否是系统组件
3. `descriptor` 组件携带的自定义数据
4. `entry_module` 入口`DLL/SO`
5. `deps` 依赖的其他组件<br>
5.1 `deps.version` 依赖的其他组件的版本描述<br>
5.2 `deps.name` 组件名称<br>
5.3 `source` 依赖组件的`pack.json`下载地址

### `deps.source`参数

URL参数:
1. `com` 组件名
2. `version` 版本描述

实际发送给下载服务的URL(实际会进行转义处理)为：

```
http://127.0.0.1/doc/test1/pack-1.0.1.json?com=test1&version=>=1.0.0
```


## `pack.json`

配置文件名大小写敏感

```
{
 "version" : "1.0.1",
 "files" : [
   {
	 "name" : "META.json",
	 "url" : "http://127.0.0.1/doc/test/1.0.1/META.json"
   },
   {
	 "name" : "test.so",
	 "url" : "http://127.0.0.1/doc/test/1.0.1/test.so"
   }
 ]  
}
```
1. `version` 外部组件版本号
2. `files` 组件包含的文件列表<br>
2.1 `name` 文件名<br>
2.2 `url` 文件的下载地址

## 下载文件的`url`参数

1. `name` 文件名

实际发送给下载服务的URL(实际会进行转义处理)为：
```
http://127.0.0.1/doc/test/1.0.1/test.so?name=test.so
```

# 外部组件的加载配置

通过配置`ServiceBox`的配置文件来加载组件.
```
component = {
    ; 存放外部组件的根目录，具体请参考前面的目录结构
    root_dir = "comp_root"
    ; 是否自动加载本地所有外部
    auto_load = true
    ; 外部组件test的配置
    test = {
        ; 要求的版本描述
        version=">=1.0.0"
        ; 远程pack.json的下载地址
        source="http://127.0.0.1/doc/test/pack-1.0.1.json"
    }
}
```

# 组件的调用

通过`ServiceContext`的函数`get_component`来获取组件:

```
//
// 参数1：组件名
// 参数2：版本描述
// 返回：组件共享指针 
//
auto com_ptr = getContext()->get_component("mycom", ">=1.0.1");
```
`Component`接口定义如下:

```
  /**
   * 获取版本号
   */
  virtual auto get_version() -> const Version * = 0;
  /**
   * 获取用户数据描述
   */
  virtual auto get_descriptor() -> const Descriptor * = 0;
  /**
   * 获取组件导出函数地址
   * @param name 导出函数名
   * @return 类实例指针
   */
  virtual auto get_func(const std::string &name) -> void * = 0;
  /**
   * 调用组件内函数
   * @param name 函数名
   * @param args 参数
   * @return 返回值
   */
  template <typename T, typename... Args>
  auto call(const std::string &name, Args... args) -> T;
```

假设组件提供了一个导出函数`bool start(const char*)`,  使用`call`调用这个函数:

```
auto result = com_ptr->call<bool>("start", "args");
```

调用`get_func`可以获取组件导出函数地址, 使用`CFunction`工具类来转换函数原型并调用:

```
using namespace kratos::component;
auto* func_addr = com_ptr->get_func("start");
auto result = CFunction<bool(const char*)>::call(func_addr, "abc");
```

# 系统组件的预置导出函数

系统组件需要导出几个预置的导出函数, 这些导出函数将被`ServiceBox`框架调用, 函数名及函数原型如下:

```
/**
 * 当需要组件初始化时调用
 * @param box 容器指针
 * @return true成功, false失败
 */
bool on_init(kratos::service::ServiceBox *);
/**
 * 当需要销毁组件时调用
 * @param box 容器指针
 * @return true成功, false失败
 */
bool on_destroy(kratos::service::ServiceBox *);
/**
 * 每帧调用
 * @param box 容器指针
 * @param ts 当前时间戳(毫秒)
 * @return true成功, false失败
 */
bool on_tick(kratos::service::ServiceBox *, std::time_t);
```