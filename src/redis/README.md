# Redis客户端

提供服务实例访问Redis server的功能。

## 使用流程

```
// 建立redis客户端实例，保留供后续使用
redis_ptr = getContext()->new_redis();
// 添加多个redis server
redis_ptr->add_host("test", "127.0.0.1", 6379, "", "");
// 启动redis客户端
redis_ptr->start();
```
```
// 逻辑主循环内调用, ms为当前时间戳(毫秒)
redis_ptr->update(ms);
```
## Redis调用
```
// 异步调用
redis_ptr->do_command("test", "SET k value", 0,
    [&](const kratos::redis::Result& result, std::uint64_t user_data) {
        if (!result->is_success()) {
            // 失败处理
        } else {
            // 成功，获取返回数据
        }
    }, 0);
```
```
// 同步（协程）调用
auto result = redis.do_command_co("test", "SET k value", 1000);
if (!result->is_success()) {
    // 失败处理
} else {
    // 成功，获取返回数据
}
```

## 获取返回

通过如下方法获取返回:
```
Result::get_return
```
详见API：
```
  virtual auto get_return(std::string &s) const -> bool = 0;
  /**
   * 从结果内获取字符串数组.
   *
   * \param [OUT] v 返回值
   * \return true或false
   */
  virtual auto get_return(std::vector<std::string> &v) const -> bool = 0;
  /**
   * 从结果内获取字符串列表.
   *
   * \param [OUT] v 返回值
   * \return true或false
   */
  virtual auto get_return(std::list<std::string> &v) const -> bool = 0;
  /**
   * 从结果内获取字符串哈希表.
   *
   * \param [OUT] m 返回值
   * \return true或false
   */
  virtual auto get_return(std::unordered_map<std::string, std::string> &m)
      const -> bool = 0;
  /**
   * 从结果内获取字符串集合.
   *
   * \param [OUT] s 返回值
   * \return true或false
   */
  virtual auto get_return(std::unordered_set<std::string> &s) const -> bool = 0;
  /**
   * 从结果内获取数量值
   *
   * \param [OUT] size 返回值
   * \return true或false
   */
  virtual auto get_return(std::size_t &size) const -> bool = 0;
  /**
   * 从结果内获取布尔值.
   *
   * \param [OUT] b 返回值
   * \return true或false
   */
  virtual auto get_return(bool &b) const -> bool = 0;
```
可以通过:
```
virtual auto get_reply() const -> void * = 0;
```
获取hiredis的原生返回数据指针redisReply* 