#pragma once

#include "call_tracer/call_tracer.hh"

namespace kratos {
namespace service {
class CallTracerKafka : public CallTracer {

public:
  CallTracerKafka();
  virtual ~CallTracerKafka();
  virtual auto start(kratos::config::BoxConfig &config) -> bool override;
  virtual auto trace(const TraceInfo& trace_info) -> void override;
  virtual auto update(std::time_t now) -> void override;
};

} // namespace service
} // namespace kratos
